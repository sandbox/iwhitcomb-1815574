CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer(s): Ian Whitcomb <iwhitcomb@w3dynamo.com>

Commerce Contribute is a very simple module designed to allow users to add a
contribution to their order during the checkout process. This module defines
a "Contribution" line item type and a checkout pane for configuring 
contributions.


INSTALLATION
------------

1. Download and install the Commerce module(http://drupal.org/project/commerce)

2. Enable commerce_checkout.

3. Copy the commerce_contribute/ directory to your sites/SITENAME/modules 
directory.

4. Enable commerce_contribute.
