<?php
/**
 * @file
 * Checkout pane callback functions for the contribute module.
 */

/**
 * Implements base_settings_form().
 */
function commerce_contribute_pane_settings_form($checkout_pane) {
  $default_currency = commerce_default_currency();
  $currency = commerce_currency_load($default_currency);
  $form = array();
  $form['commerce_contribute_min_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum Amount'),
    '#default_value' => variable_get('commerce_contribute_min_amount', '0.00'),
    '#field_suffix' => $currency['code'],
    '#required' => TRUE,
  );
  $form['commerce_contribute_designations'] = array(
    '#type' => 'textarea',
    '#title' => t('Designation Options'),
    '#default_value' => variable_get('commerce_contribute_designations', ''),
  );
  return $form;
}

/**
 * Validate contribute pane settings form submissions.
 */
function commerce_contribute_pane_settings_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['commerce_contribute_min_amount'])) {
    form_set_error('commerce_contribute_min_amount', t('Minumum amount must be numeric.'));
  }
}

/**
 * Process contribute pane settings form submissions.
 */
function commerce_contribute_pane_settings_form_submit($form, &$form_state) {
  variable_set('commerce_contribute_designations', $form_state['values']['commerce_contribute_designations']);
  variable_set('commerce_contribute_min_amount', $form_state['values']['commerce_contribute_min_amount']);
}

/**
 * Checkout pane callback.
 */
function commerce_contribute_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $default_currency = commerce_default_currency();
  $currency = commerce_currency_load($default_currency);
  $pane_form = array();

  $options = list_extract_allowed_values(variable_get('commerce_contribute_designations'), 'list_text', FALSE);
  // Allow other modules to set options as well.
  drupal_alter('commerce_contribute_designation', $options);

  $default_value = variable_get('commerce_contribute_min_amount', '0.00');
  // Set the default to any contributions already in the cart.
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if ($line_item_wrapper->type->value() == 'contribution') {
      $default_value = $line_item_wrapper->quantity->value() * commerce_currency_amount_to_decimal($line_item_wrapper->commerce_unit_price->amount->value(), $line_item_wrapper->commerce_unit_price->currency_code->value());
    }
  }

  if ($options) {
    $pane_form['designation'] = array(
      '#type' => 'select',
      '#title' => t('Designation'),
      '#options' => $options,
    );
  }
  $pane_form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#required' => TRUE,
    '#field_suffix' => $currency['code'],
    '#default_value' => $default_value,
    '#description' => t('Minimum !min_amount!currency_code', array(
      '!min_amount' => variable_get('commerce_contribute_min_amount', '0.00'),
      '!currency_code' => $currency['code'],
    )),
  );
  return $pane_form;
}

/**
 * Process contribute pane submissions.
 */
function commerce_contribute_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  // Select any line_items in the order first so we don't duplicate them.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if ($line_item_wrapper->type->value() == 'contribution') {
      $line_item = $line_item_wrapper->value();
    }
  }

  if ($form_state['values']['checkout_contribute']['amount'] > 0 && !empty($form_state['values']['checkout_contribute']['amount'])) {

    $default_currency_code = commerce_default_currency();
    if ($balance = commerce_payment_order_balance($order)) {
      $default_currency_code = $balance['currency_code'];
    }

    if (isset($line_item)) {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

      $line_item_wrapper->commerce_unit_price->amount = ($form_state['values']['checkout_contribute']['amount'] * 100);
      $line_item_wrapper->commerce_unit_price->currency_code = $default_currency_code;

      $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
        NULL,
        'contribution',
        $line_item_wrapper->commerce_unit_price->value(),
        TRUE,
        FALSE
      );

      $line_item_wrapper->save();
    }
    else {
      $line_item = commerce_line_item_new('contribution', $order->order_id);
      $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

      $line_item_wrapper->line_item_label = t('Contribution');
      if (!empty($form_state['values']['checkout_contribute']['designation'])) {
        $line_item_wrapper->commerce_contribute_designation = $form_state['values']['checkout_contribute']['designation'];
      }
      $line_item_wrapper->quantity = 1;

      $line_item_wrapper->commerce_unit_price->amount = ($form_state['values']['checkout_contribute']['amount'] * 100);
      $line_item_wrapper->commerce_unit_price->currency_code = $default_currency_code;

      $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
        $line_item_wrapper->commerce_unit_price->value(),
        'contribution',
        $line_item_wrapper->commerce_unit_price->value(),
        TRUE,
        FALSE
      );

      $line_item_wrapper->save();
      $order_wrapper->commerce_line_items[] = $line_item;

    }

    commerce_line_item_save($line_item);

    commerce_order_save($order);
  }
}
